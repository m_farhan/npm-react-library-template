import jsx from 'rollup-plugin-jsx'
import postcss from 'rollup-plugin-postcss'
import pkg from './package.json'

export default {
  input: 'src/index.jsx',
  output: [
    {
      file: pkg.main,
      format: 'cjs',
      exports: 'named',
      sourcemap: true,
      strict: false,
    },
  ],
  plugins: [
    postcss({
      extract: false,
      modules: true,
      use: ['sass'],
    }),
    jsx({ factory: 'React.createElement' }),
  ],
  external: ['react', 'react-dom'],
}
