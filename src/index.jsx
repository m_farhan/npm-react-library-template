import React from 'react'
import styles from './styles.module.scss'
var test = { a: 2, b: 3, c: 5 }
var d = { ...test }
const Package = (props) => (
  <div className={styles.package} {...props}>
    <h2>Do cool stuff</h2>
  </div>
)

export default Package
